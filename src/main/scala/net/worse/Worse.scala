package net.worse

import cats.implicits._
import io.circe._
import io.circe.syntax._
import io.circe.generic.semiauto._

sealed trait Thing
object Thing {

  implicit val encoder: Encoder[Thing] = Encoder.instance {
    case a: A => a.asJson
    case b: B => b.asJson
    case c: C => c.asJson
  }

  implicit val decoder: Decoder[Thing] = List[Decoder[Thing]](
    Decoder[A].widen,
    Decoder[B].widen,
    Decoder[C].widen
  ).reduceLeft(_ or _)

  case class A(a: Int, b: String) extends Thing
  object A {
    implicit val codec: Codec[A] = deriveCodec[A]
  }
  case class B(a: Int, b: Boolean) extends Thing
  object B {
    implicit val codec: Codec[B] = deriveCodec[B]
  }
  case class C(a: Int, b: String) extends Thing
  object C {
    implicit val codec: Codec[C] = deriveCodec[C]
  }
}