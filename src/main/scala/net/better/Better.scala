package net.better

import cats.implicits._
import io.circe._
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import io.circe.generic.extras.semiauto._

sealed trait Thing
object Thing {

  implicit val codec: Codec[Thing] = {
    implicit val config: Configuration =
      Configuration.default.copy(transformConstructorNames = _.toLowerCase)

    deriveConfiguredCodec[Thing]
  }

  case class A(a: Int, b: String) extends Thing
  case class B(a: Int, b: Boolean) extends Thing
  case class C(a: Int, b: String) extends Thing
}