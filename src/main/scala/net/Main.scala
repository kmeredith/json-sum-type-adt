package net

import io.circe.syntax._
import io.circe._

object Main {
  def main(args: Array[String]): Unit = {

    val c: net.better.Thing = net.better.Thing.C(42, "foobar")
    val json: Json = c.asJson
    val decoded: Either[DecodingFailure, net.better.Thing] = json.as[net.better.Thing]

    println(">>> json: "+ json.spaces2)
    println(">>> decoded: "+ decoded)

  }
}
